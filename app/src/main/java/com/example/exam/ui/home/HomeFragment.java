package com.example.exam.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.exam.R;
import com.example.exam.databinding.FragmentHomeBinding;
import com.example.exam.utils.DBHandler;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;

    private DBHandler dbHandler ;

    public HomeFragment(){

    }
    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        dbHandler = new DBHandler(getContext());

        final Button button = root.findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dbHandler.addNewActivity("Button 1 Pressed", new SimpleDateFormat("dd/MM/yyyy HH:mm aa").format(new Date()));
            }
        });

        final Button button2 = root.findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dbHandler.addNewActivity("Button 2 Pressed", new SimpleDateFormat("dd/MM/yyyy HH:mm aa").format(new Date()));
            }
        });

        final Button button3 = root.findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dbHandler.addNewActivity("Button 3 Pressed", new SimpleDateFormat("dd/MM/yyyy HH:mm aa").format(new Date()));
            }
        });

        final Button button4 = root.findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dbHandler.addNewActivity("Button 4 Pressed", new SimpleDateFormat("dd/MM/yyyy HH:mm aa").format(new Date()));
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}