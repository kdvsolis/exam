package com.example.exam.models;

public class ButtonActivity {

    private String activity;
    private String datetime;
    private int id;

    // creating getter and setter methods
    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // constructor
    public ButtonActivity(String _activity, String _datetime) {
        this.activity = _activity;
        this.datetime = _datetime;
    }
}
