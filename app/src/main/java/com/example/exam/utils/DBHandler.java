package com.example.exam.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.exam.models.ButtonActivity;

import java.util.ArrayList;

public class DBHandler extends SQLiteOpenHelper {
    private static final String DB_NAME = "test_db";
    private static final int DB_VERSION = 1;
    private static final String TABLE_NAME = "activity";
    private static final String ID_COL = "id";
    private static final String ACTIVITY = "activity";
    private static final String DATETIME = "duration";

    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ACTIVITY + " TEXT,"
                + DATETIME + " TEXT)";
        db.execSQL(query);
    }

    public void addNewActivity(String activity, String datetime) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ACTIVITY, activity);
        values.put(DATETIME, datetime);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public ArrayList<String> readActivity() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor buttonActivity = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);

        ArrayList<String> activityArrayList = new ArrayList<>();

        if (buttonActivity.moveToFirst()) {
            do {
                activityArrayList.add(buttonActivity.getString(1) + "\n" + buttonActivity.getString(2));
            } while (buttonActivity.moveToNext());
        }
        buttonActivity.close();
        return activityArrayList;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
